
// Codigo de Semáforo
// 07/12/2023
// V 0.1
// Las conexiones son las siguientes:
// * Botón conectado en pin 3 con resistencia de PULLUP habilitada
// * Rojo conectado en el pin 11
// * Amarillo conectado en el pin 12
// * Verde conectado en el pin 13

//Desarrollo del proyecto en PILARES Frida Kahlo y Agrícola Pantitlán
// Participaron

// * Pilar
// * Estela
// * Armando
// * Amadeo
// * Ricardo
// * Max
// * Pavel electroemprende19@riseup.net


// Este proyecto se ha desarrollado desde la perspectiva de hardware y software libres
// con Licencia Creative Commons
// Puedes aportar ideas o consultar los diagramas esquemáticos y archivos de PCB en 
// gitlab.com/dpw19/semaforo
//

const int PIN_BOTON = 3;
const int green=13;
const int yellow = 12;
const int red = 11;






void setup()
{
  pinMode(green,OUTPUT);
  pinMode(red,OUTPUT);
  pinMode(yellow,OUTPUT);

  pinMode(PIN_BOTON, INPUT_PULLUP);
}

void loop(){
  verde(1);  // secuencia de led verde
  amarillo(3); //secuencia amarillo
  rojo(1); // secuencia rojo

  
  verde (2); // secuencia de led verde
  amarillo(3); //secuencia amarillo
  rojo(2); // secuencia rojo
}

void verde(int tiempo){
  // secuencia del led verde
  // recibe el parámetro de tiempo en minutos
  digitalWrite(green, HIGH);
  delay(tiempo*10000); // espera un minuto
  digitalWrite(green, LOW);
  delay(500); // espera por medio segundo
  digitalWrite(green, HIGH);
  delay(500); // espera por medio segundo
  digitalWrite(green, LOW);
  delay(500); // espera por medio segundo
  digitalWrite(green, HIGH);
  delay(500);// espera por medio segundo
  digitalWrite(green,LOW);
  delay(500);// espera por medio segundo
}

void amarillo(int tiempo){
  //secuencia de amarillo
  // recibe el parámetro de tiempo en segundos
  digitalWrite(yellow,HIGH);
  delay (tiempo*1000);// espera por dos segundo     
  digitalWrite(yellow,LOW);
}

void rojo(int tiempo){
  //secuencia del led rojo
  //recibe el parámetro de tempo en minutos 
  digitalWrite(red,HIGH);
  delay(tiempo*10000);//espera un minuto
  digitalWrite(red,LOW);     
}
